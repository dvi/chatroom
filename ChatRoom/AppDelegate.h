//
//  AppDelegate.h
//  ChatRoom
//
//  Created by sashadiv on 06/01/16.
//  Copyright © 2016 sashadiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

