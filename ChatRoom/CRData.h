//
//  CRData.h
//  ChatRoom
//
//  Created by sashadiv on 13/01/16.
//  Copyright © 2016 sashadiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessages.h"

static NSString *const kSenderId = @"kSenderId";
static NSString *const kSenderDisplayName = @"kSenderDisplayName";

@interface CRData : NSObject

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

- (JSQMessage *)addPhotoMediaMessage;

@end
