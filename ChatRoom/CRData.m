//
//  CRData.m
//  ChatRoom
//
//  Created by sashadiv on 13/01/16.
//  Copyright © 2016 sashadiv. All rights reserved.
//

#import "CRData.h"

@implementation CRData

- (instancetype)init {
    if ((self = [super init])) {
        _messages = [[NSMutableArray alloc]init];
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    }
    return self;
}

- (JSQMessage *)addPhotoMediaMessage
{
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"goldengate"]];
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:kSenderId
                                                   displayName:kSenderDisplayName
                                                         media:photoItem];
    return photoMessage;
}

@end
